local wk = require("which-key")
local map = vim.api.nvim_set_keymap
-- function set_keymap(mode, from, to)
-- 	local opts = { noremap = true, silent = false }
-- 	map(mode, from, to, opts)
-- end

map("n", "<Space>", "", {})
vim.g.mapleader = " "

wk.register({
  ["<leader>"] = { "<cmd>Cheatsheet<CR>", "Open cheatsheet" },
  b = {
    name = "+buffers",
    b = { "<cmd>Telescope buffers<CR>", "Switch to buffer..." },
    n = { "<cmd>bnext<CR>", "Next buffer" },
    N = { "<cmd>bprev<CR>", "Previous buffer" },
  },
  ["<tab>"] = { "<cmd>b#<CR>", "Switch to last used buffer" },
  w = {
    name = "+windows",
    s = { "<cmd>sbprevious<CR>", "Split window below" },
    v = { "<cmd>vert sbprevious<CR>", "Split window to the right" },
    w = { "<cmd>wincmd w<CR>", "Cycle to next window" },
  },
  p = {
    name = "+project",
    f = { "<cmd>Telescope find_files<cr>", "Find files in the project" },
    p = { "<cmd>Telescope project<CR>", "Open project" },
  },
  f = {
    name = "+files",
    r = { "<cmd>Telescope frecency<CR>", "Open recent files" },
    f = { "<cmd>Telescope file_browser<CR>", "Find file" },
    s = { "<cmd>Telescope session-lens search_session<CR>", "Restore session" },
  },
  y = {
    name = "+yank",
    l = { "<cmd>Telescope neoclip<CR>", "List clipboard items" },
  },
  s = {
    name = "+search",
    s = { "<cmd>Telescope current_buffer_fuzzy_find<CR>", "Search in current buffer" },
    p = { "<cmd>Telescope live_grep<CR>", "Search in the current project" },
  },
  j = {
    name = "+jump to",
    w = { "<cmd>HopWord<CR>", "Jump to word" },
    c = { "<cmd>HopChar1<CR>", "Jump to char" },
    b = { "<cmd>HopChar2<CR>", "Jump to binome" },
    l = { "<cmd>HopLine<CR>", "Jump to line" },
  },
  g = {
    name = "+git",
    b = { "<cmd>Gitsigns toggle_current_line_blame<CR>", "Git blame" },
  },
  q = {
    name = "+quit",
    q = { "<cmd>quitall<CR>", "Quit all" },
  },
}, { prefix = "<leader>" })
wk.register({
  r = { "<cmd>Telescope lsp_references<CR>", "Go to references" },
}, { prefix = "g" })

wk.register({
  j = {
    name = "+jump to",
    w = { "<cmd>HopWord<CR>", "Jump to word" },
    c = { "<cmd>HopChar1<CR>", "Jump to char" },
    b = { "<cmd>HopChar2<CR>", "Jump to binome" },
    l = { "<cmd>HopLine<CR>", "Jump to line" },
  },
}, { mode = "v", prefix = "<leader>" })
wk.register({
  j = {
    name = "+jump to",
    w = { "<cmd>HopWord<CR>", "Jump to word" },
    c = { "<cmd>HopChar1<CR>", "Jump to char" },
    b = { "<cmd>HopChar2<CR>", "Jump to binome" },
    l = { "<cmd>HopLine<CR>", "Jump to line" },
  },
}, { mode = "o", prefix = "<leader>" })
