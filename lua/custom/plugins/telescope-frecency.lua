return {
  "nvim-telescope/telescope-frecency.nvim",
  keys = {
    { "<leader>fr", "<cmd>Telescope frecency<CR>", desc = "Open recent files" }
  },
  config = function()
    require("telescope").load_extension("frecency")
  end
}
