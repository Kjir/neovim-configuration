return {
  "nvim-telescope/telescope-file-browser.nvim",
  config = function()
    require("telescope").load_extension("file_browser")
  end,
  keys = { { "<leader>ff", "<cmd>Telescope file_browser<CR>", desc = "[F]ind [f]ile" },
    {
      "<leader>fa",
      "<cmd>Telescope file_browser path=%:p:h select_buffer=true<CR>",
      desc = "[F]ind [a]djacent file in the same directory" } }
}
