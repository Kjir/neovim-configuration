return {
  "sindrets/diffview.nvim",
  dependencies = { "nvim-lua/plenary.nvim" },
  keys = { { "<leader>dc", "<cmd>DiffviewOpen<CR>", desc = "[D]iff [C]urrent Index" } }
}
