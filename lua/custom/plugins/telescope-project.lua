return {
  "nvim-telescope/telescope-project.nvim",
  keys = {
    { "<leader>pp", "<cmd>Telescope project<CR>", desc = "Open project" }
  },
  config = function()
    require("telescope").load_extension("project")
  end
}
