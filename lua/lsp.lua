local nvim_lsp = require("lspconfig")
local nvim_lsp_configs = require("lspconfig.configs")
local nvim_lsp_util = require("lspconfig.util")

require("mason").setup()
local mason_lsp = require("mason-lspconfig")
mason_lsp.setup({ ensure_installed = { "pyright", "jsonls", "ts_ls", "volar", "yamlls", "efm" } })

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
  local function buf_set_option(...)
    vim.api.nvim_buf_set_option(bufnr, ...)
  end

  -- Enable completion triggered by <c-x><c-o>
  buf_set_option("omnifunc", "v:lua.vim.lsp.omnifunc")

  -- See `:help vim.lsp.*` for documentation on any of the below functions
  local wk = require("which-key")
  wk.register({
    ["<leader>"] = {
      w = {
        name = "+window/workspace",
        a = { "<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>", "Add workspace folder" },
        r = { "<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>", "Remove workspace folder" },
        l = {
          "<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>",
          "List workspace folders",
        },
      },
      r = {
        name = "+refactor",
        r = { "<cmd>lua vim.lsp.buf.rename()<CR>", "Rename symbol" },
      },
      l = {
        name = "+lsp",
        a = { "<cmd>lua vim.lsp.buf.code_action()<CR>", "Code actions" },
        d = { "<cmd>Telescope lsp_document_diagnostics<CR>", "Document diagnostics" },
        e = { "<cmd>lua vim.diagnostic.open_float()<CR>", "Show error on the current line" },
        f = {
          "<cmd>lua vim.lsp.buf.format { async = true }<CR>",
          "Format current buffer",
        },
      },
    },
    g = {
      name = "+go to",
      D = { "<cmd>lua vim.lsp.buf.declaration()<CR>", "Go to declaration" },
      d = { "<cmd>lua vim.lsp.buf.definition()<CR>", "Go to definition" },
      i = { "<cmd>lua vim.lsp.buf.implementation()<CR>", "Go to implementation" },
    },
    K = { "<cmd>lua vim.lsp.buf.hover()<CR>", "Show symbol information" },
    ["<C-k>"] = { "<cmd>lua vim.lsp.buf.signature_help()<CR>", "Show function signature" },
    ["["] = {
      d = { "<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>", "Go to previous error" },
    },
    ["]"] = {
      d = { "<cmd>lua vim.lsp.diagnostic.goto_next()<CR>", "Go to next error" },
    },
  }, { buffer = bufnr })
end

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require("cmp_nvim_lsp").default_capabilities(capabilities)
--Enable (broadcasting) snippet capability for completion
capabilities.textDocument.completion.completionItem.snippetSupport = true

local initialized_servers = {}

local servers_with_snippets = { "html", "cssls" }
for _, lsp in ipairs(servers_with_snippets) do
  nvim_lsp[lsp].setup({
    on_attach = on_attach,
    flags = {
      debounce_text_changes = 150,
    },
    capabilities = capabilities,
  })
  initialized_servers[lsp] = true
end

nvim_lsp.jsonls.setup({
  capabilities = capabilities,
  on_attach = on_attach,
  commands = {
    Format = {
      function()
        vim.lsp.buf.range_formatting({}, { 0, 0 }, { vim.fn.line("$"), 0 })
      end,
    },
  },
})
initialized_servers.jsonls = true

-- nvim_lsp.sumneko_lua.setup({
--   cmd = { "lua-language-server" },
--   on_attach = on_attach,
--   settings = {
--     Lua = {
--       runtime = {
--         version = "LuaJIT",
--       },
--       diagnostics = {
--         globals = { "vim" },
--       },
--       workspace = {
--         library = vim.api.nvim_get_runtime_file("", true),
--       },
--       telemetry = {
--         enable = false,
--       },
--     },
--   },
--   capabilities = capabilities,
-- })
-- initialized_servers.sumneko_lua = true

-- Volar configuration
--
-- local function on_new_config(new_config, new_root_dir)
--   local function get_typescript_server_path(root_dir)
--     local project_root = nvim_lsp_util.find_node_modules_ancestor(root_dir)
--     return project_root
--         and (nvim_lsp_util.path.join(project_root, "node_modules", "typescript", "lib", "tsserverlibrary.js"))
--         or ""
--   end
--
--   if
--       new_config.init_options
--       and new_config.init_options.typescript
--       and new_config.init_options.typescript.serverPath == ""
--   then
--     new_config.init_options.typescript.serverPath = get_typescript_server_path(new_root_dir)
--   end
-- end

-- local volar_cmd = { "vue-language-server", "--stdio" }
-- local volar_root_dir = nvim_lsp_util.root_pattern("package.json")

-- nvim_lsp_configs.volar_api = {
--   default_config = {
--     cmd = volar_cmd,
--     root_dir = volar_root_dir,
--     on_new_config = on_new_config,
--     filetypes = { "vue" },
--     -- If you want to use Volar's Take Over Mode (if you know, you know)
--     --filetypes = { 'typescript', 'javascript', 'javascriptreact', 'typescriptreact', 'vue', 'json' },
--     init_options = {
--       typescript = {
--         serverPath = "",
--       },
--       languageFeatures = {
--         implementation = true, -- new in @volar/vue-language-server v0.33
--         references = true,
--         definition = true,
--         typeDefinition = true,
--         callHierarchy = true,
--         hover = true,
--         rename = true,
--         renameFileRefactoring = true,
--         signatureHelp = true,
--         codeAction = true,
--         workspaceSymbol = true,
--         completion = {
--           defaultTagNameCase = "both",
--           defaultAttrNameCase = "kebabCase",
--           getDocumentNameCasesRequest = false,
--           getDocumentSelectionRequest = false,
--         },
--       },
--     },
--   },
-- }
-- nvim_lsp.volar_api.setup({})
-- initialized_servers.volar_api = true

-- nvim_lsp_configs.volar_doc = {
--   default_config = {
--     cmd = volar_cmd,
--     root_dir = volar_root_dir,
--     on_new_config = on_new_config,
--
--     filetypes = { "vue" },
--     -- If you want to use Volar's Take Over Mode (if you know, you know):
--     --filetypes = { 'typescript', 'javascript', 'javascriptreact', 'typescriptreact', 'vue', 'json' },
--     init_options = {
--       typescript = {
--         serverPath = "",
--       },
--       languageFeatures = {
--         implementation = true, -- new in @volar/vue-language-server v0.33
--         documentHighlight = true,
--         documentLink = true,
--         codeLens = { showReferencesNotification = true },
--         -- not supported - https://github.com/neovim/neovim/pull/15723
--         semanticTokens = false,
--         diagnostics = true,
--         schemaRequestService = true,
--       },
--     },
--   },
-- }
-- nvim_lsp.volar_doc.setup({})
-- initialized_servers.volar_doc = true

-- nvim_lsp_configs.volar_html = {
--   default_config = {
--     cmd = volar_cmd,
--     root_dir = volar_root_dir,
--     on_new_config = on_new_config,
--
--     filetypes = { "vue" },
--     -- If you want to use Volar's Take Over Mode (if you know, you know), intentionally no 'json':
--     --filetypes = { 'typescript', 'javascript', 'javascriptreact', 'typescriptreact', 'vue' },
--     init_options = {
--       typescript = {
--         serverPath = "",
--       },
--       documentFeatures = {
--         selectionRange = true,
--         foldingRange = true,
--         linkedEditingRange = true,
--         documentSymbol = true,
--         -- not supported - https://github.com/neovim/neovim/pull/13654
--         documentColor = false,
--         documentFormatting = {
--           defaultPrintWidth = 100,
--         },
--       },
--     },
--   },
-- }
-- nvim_lsp.volar_html.setup({})
-- initialized_servers.volar_html = true

nvim_lsp.efm.setup({
  init_options = { documentFormatting = true },
  rootMarkers = { '.git/' },
  settings = {
    languages = {
      r = {
        formatCommand = string.format(
          [[R --slave --no-restore --no-save -e 'con=file("stdin");output=styler::style_text(readLines(con));close(con);print(output, colored=FALSE)']])
      },
      rmd = {
        formatCommand = "echo no"
      }
    },
  },
})
initialized_servers.efm = true

local installed_servers = mason_lsp.get_installed_servers()
for _, lsp in ipairs(installed_servers) do
  if not initialized_servers[lsp] then
    nvim_lsp[lsp].setup({
      on_attach = on_attach,
      capabilities = capabilities,
      flags = {
        debounce_text_changes = 150,
      },
    })
  end
end
